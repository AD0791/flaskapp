import sqlite3

def connection():
    conn = sqlite3.connect('PythonProgramming.db')
    c = conn.cursor()
    return c,conn
